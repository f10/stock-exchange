package de.tu_berlin.cit.vs.aws.stockexchange;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

import de.tu_berlin.cit.vs.aws.stockexchange.messages.*;

/**
 * The Class SimpleStockExchange.
 * 
 * @author Florian Beckmann and others- florianbeckmann@gmx.net
 */
public class SimpleStockExchange {

	/** Logger for this class. */
	private static final Logger logger = LoggerFactory.getLogger(SimpleStockExchange.class);

	/** The Constant REGISTER_QUEUE_NAME. */
	public static final String REGISTER_QUEUE_NAME = ""; // enter the name of
															// the registration
															// queue here

	/** The amazon sqs client. */
	private static AmazonSQS amazonSQSClient = null;

	/** The stocks. */
	private Map<StockName, Stock> stocks = new HashMap<StockName, Stock>();

	/** The main io loop. */
	private StockExchangeIOLoop mainIOLoop = null;

	/** The register queue url. */
	private String registerQueueUrl;

	/** The stock exchange io loops. */
	private Map<String, StockExchangeIOLoop> stockExchangeIOLoops;

	/** The stocks subscriptions. */
	private Map<StockName, List<String>> stocksSubscriptions;

	/**
	 * The Enum StockName.
	 */
	public enum StockName {

		AAPL, GOOG, QCOM, EBAY, MSFT, ORCL, AMZN
	};

	/**
	 * Instantiates a new simple stock exchange.
	 */
	public SimpleStockExchange() {

		stockExchangeIOLoops = new ConcurrentHashMap<String, StockExchangeIOLoop>();
		// init stocks;
		initStocks();
	}

	/**
	 * TODO: IMPLEMENT
	 * 
	 * do initialization stuff here.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void start() throws IOException {

		// init client
		amazonSQSClient = new AmazonSQSClient(new PropertiesCredentials(
				SimpleStockExchange.class.getResourceAsStream("/AwsCredentials.properties")));
		amazonSQSClient.setRegion(com.amazonaws.regions.Region.getRegion(Regions.EU_WEST_1));

		// Create the register queue
		logger.info("{}", "Creating a new SQS queue called " + StockExchangeMessage.STOCK_EXCHANGE + "."); //$NON-NLS-1$ //$NON-NLS-2$
		CreateQueueRequest createQueueRequest = new CreateQueueRequest(StockExchangeMessage.STOCK_EXCHANGE);
		registerQueueUrl = amazonSQSClient.createQueue(createQueueRequest).getQueueUrl();

		logger.info("Done"); //$NON-NLS-1$ //$NON-NLS-2$

		// List queues
		logger.info("Listing all stock exchange queues."); //$NON-NLS-1$ //$NON-NLS-2$

		for (String queueUrl : amazonSQSClient.listQueues().getQueueUrls()) {
			logger.info("QueueUrl: {}", queueUrl); //$NON-NLS-1$ //$NON-NLS-2$
		}

		logger.info("{}", "Starting Main IO Thread ... "); //$NON-NLS-1$ //$NON-NLS-2$
		mainIOLoop = new StockExchangeIOLoop();
		mainIOLoop.start();

		logger.info("Done"); //$NON-NLS-1$ //$NON-NLS-2$

		logger.info("Finished starting up ... Press any key to stop program. \n"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * TODO IMPLEMENTStockExchangeIOLoop
	 * 
	 * clean up here, destroy running threads, queues ...
	 */
	private void stop() {

		logger.info("Deleting the registration queue."); //$NON-NLS-1$ //$NON-NLS-2$
		amazonSQSClient.deleteQueue(new DeleteQueueRequest(registerQueueUrl));

		logger.info("Deleting client queues."); //$NON-NLS-1$ //$NON-NLS-2$
		for (String clientName : stockExchangeIOLoops.keySet()) {
			removeClient(clientName);
		}

		logger.info("Done"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Buy stock.
	 * 
	 * @param stockName
	 *            the stock name
	 * @param count
	 *            the count
	 */
	protected synchronized void buyStock(String stockName, int count) {

		Stock stock = stocks.get(StockName.valueOf(stockName));
		if (null != stock && stock.getAvailableCount() - count >= 0) {
			stock.setAvailableCount(stock.getAvailableCount() - count);
			updateSubscribers(stockName);
		}
		else {
			throw new RuntimeException("Buy " + count + " " + stockName + " stocks not possible: "
					+ (null == stock ? "stockName doesn't exist" : stock.getAvailableCount() + " stocks left."));
		}

	}

	/**
	 * Sell stock.
	 * 
	 * @param stockName
	 *            the stock name
	 * @param count
	 *            the count
	 */
	protected synchronized void sellStock(String stockName, int count) {

		Stock stock = stocks.get(StockName.valueOf(stockName));
		if (null != stock && stock.getAvailableCount() + count <= stock.getStockCount()) {
			stock.setAvailableCount(stock.getAvailableCount() + count);
			updateSubscribers(stockName);
		}
		else {
			throw new RuntimeException(
					"Sell "
							+ count
							+ " "
							+ stockName
							+ " stocks not possible:"
							+ (null == stock ? " stockName doesn't exist"
									: "amount available + offered stocks exceed stock count of "
											+ stock.getStockCount()
											+ " by "
											+ (stock.getAvailableCount() + count - stock.getStockCount() + ". YOU'RE CHEATING!")));
		}
	}

	/**
	 * Removes the client.
	 * 
	 * @param clientName
	 *            the client name
	 */
	protected synchronized void removeClient(String clientName) {
		logger.info("Removing client {}", clientName);
		StockExchangeIOLoop stockExchangeIOLoop = stockExchangeIOLoops.get(clientName);
		logger.info("Sending client handler for client {} message to stop.", clientName);
		stockExchangeIOLoop.setStop(true);
		stockExchangeIOLoop.cleanUp();
		stockExchangeIOLoops.remove(clientName);
		unsubscribeAll(clientName);
		logger.info("Client {} removed and all subcriptions canceled.", clientName);
	}

	/**
	 * Subscribe stock.
	 * 
	 * @param clientName
	 *            the client name
	 * @param stockName
	 *            the stock name
	 */
	protected void subscribeStock(String clientName, String stockName) {
		// this list instance is thread safe
		List<String> stockSubscriptions = stocksSubscriptions.get(StockName.valueOf(stockName));
		stockSubscriptions.add(clientName);
		logger.info("Client {} subscribed {}", clientName, stockName);
	}

	/**
	 * Unsubscribe stock.
	 * 
	 * @param clientName
	 *            the client name
	 * @param stockName
	 *            the stock name
	 */
	protected void unsubscribeStock(String clientName, String stockName) {
		// this list instance is thread safe
		List<String> stockSubscriptions = stocksSubscriptions.get(StockName.valueOf(stockName));
		stockSubscriptions.remove(stockName);
		logger.info("Client {} unsubscribed {}", clientName, stockName);

	}

	/**
	 * Update subscribers.
	 * 
	 * @param stocksName
	 *            the stocks name
	 */
	protected void updateSubscribers(String stocksName) {

		logger.info("Sending update message to all subscribers of {}", stocksName);
		StockName stockName = StockName.valueOf(stocksName);
		if (null != stockName) {
			List<String> stockSubscriptions = stocksSubscriptions.get(stockName);
			Stock updateStock = stocks.get(stockName);

			for (String clientName : stockSubscriptions) {
				StockExchangeIOLoop subscriber = stockExchangeIOLoops.get(clientName);
				if (null != subscriber) {

					StockUpdateMessage stockUpdateMessage = new StockUpdateMessage(clientName, updateStock);
					subscriber.sendStockUpdateMessage(stockUpdateMessage);
					logger.info("Update message for stock {} sent to client {}", stocksName, clientName);
				}
			}
		}
	}

	/**
	 * Unsubscribe all.
	 * 
	 * @param clientName
	 *            the client name
	 */
	protected void unsubscribeAll(String clientName) {

		int count = 0;
		for (List<String> stockSubscriptions : stocksSubscriptions.values()) {
			if (stockSubscriptions.contains(clientName)) {
				stockSubscriptions.remove(clientName);
				count++;
			}
		}
		logger.info("Unsubcribed client {} for all ({}) subscribed stocks", clientName, count);
	}

	/**
	 * Gets the stocks.
	 * 
	 * @return the stocks
	 */
	protected LinkedList<Stock> getStocks() {
		LinkedList<Stock> result = new LinkedList<Stock>();
		for (Stock stock : stocks.values())
			result.add(stock);

		return result;
	}

	/**
	 * Gets the stock.
	 * 
	 * @param stockName
	 *            the stock name
	 * @return the stock
	 */
	protected Stock getStock(String stockName) {
		return stocks.get(stockName);
	}

	/**
	 * Checks if is stock known.
	 * 
	 * @param stockName
	 *            the stock name
	 * @return true, if is stock known
	 */
	public boolean isStockKnown(String stockName) {
		return stocks.containsKey(stockName);
	}

	/**
	 * The Class StockExchangeIOLoop.
	 */
	private class StockExchangeIOLoop extends Thread {

		/** Logger for this class. */
		private final Logger logger = LoggerFactory.getLogger(StockExchangeIOLoop.class);

		// the client's input queue is the server's output queue (write)
		/** The input queue url. */
		private String inputQueueUrl;

		// the cliene's output queue us the server's input queue (read)
		/** The output queue url. */
		private String outputQueueUrl;

		/** The client name. */
		private String clientName;

		/** The stop. */
		private boolean stop;

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {

			if (null != clientName) {
				logger.info("StockExchangeIOLoop for client {} started.", clientName);
			}
			if (null == outputQueueUrl) {
				// if outputQueueUrl is not set means this is the
				// "register queue loop"
				outputQueueUrl = registerQueueUrl;
			}
			else {
				// send welcome message to client in form of stock list
				StockListMessage stockListMessage = new StockListMessage(getStocks(), clientName);
				amazonSQSClient.sendMessage(new SendMessageRequest(inputQueueUrl, StockExchangeMessage
						.encode(stockListMessage)));
			}
			int counter = 0;
			while (!interrupted() && !isStop()) {
				try {

					// receive all messages in queue currently available;
					List<Message> messages = amazonSQSClient.receiveMessage(new ReceiveMessageRequest(outputQueueUrl))
							.getMessages();

					if (messages.isEmpty()) {
						try {
							if (counter % 10 == 0) {
								// print idle message after ten cycles.
								logger.info("Idle...");
							}

							sleep(1000);
							counter++;
						}
						catch (InterruptedException e) {
							logger.error("run()", e); //$NON-NLS-1$

							break;
						}
						continue;
					}

					// handle messages
					logger.info("{} messages in queue found.", messages.size());
					for (Message message : messages) {
						try {
							StockExchangeMessage stockExchangeMessage = StockExchangeMessage.decode(message.getBody());

							// decode message
							if (null == stockExchangeMessage) {
								logger.error("Error decoding client message ..."); //$NON-NLS-1$
								continue;
							}

							if (stockExchangeMessage instanceof SystemRegisterMessage) {
								SystemRegisterMessage systemRegisterMessage = (SystemRegisterMessage) stockExchangeMessage;
								registerClient(systemRegisterMessage);
							}
							else if (stockExchangeMessage instanceof StockBuyMessage) {
								StockBuyMessage stockBuyMessage = (StockBuyMessage) stockExchangeMessage;
								buyStock(stockBuyMessage.getStockName(), stockBuyMessage.getAmount());

							}
							else if (stockExchangeMessage instanceof StockListMessage) {
								StockListMessage stockListMessage = (StockListMessage) stockExchangeMessage;

								// send same message back modified
								stockListMessage.setClientName(clientName);
								stockListMessage.setStocks(getStocks());
								amazonSQSClient.sendMessage(new SendMessageRequest(inputQueueUrl, StockExchangeMessage
										.encode(stockListMessage)));

							}
							else if (stockExchangeMessage instanceof StockSellMessage) {
								StockSellMessage stockSellMessage = (StockSellMessage) stockExchangeMessage;
								sellStock(stockSellMessage.getStockName(), stockSellMessage.getAmount());
							}
							else if (stockExchangeMessage instanceof StockSubscribeMessage) {
								StockSubscribeMessage stockSubscribeMessage = (StockSubscribeMessage) stockExchangeMessage;
								subscribeStock(stockSubscribeMessage.getClientName(),
										stockSubscribeMessage.getStockName());

							}
							else if (stockExchangeMessage instanceof StockUnsubscribeMessage) {
								StockUnsubscribeMessage stockUnsubscribeMessage = (StockUnsubscribeMessage) stockExchangeMessage;
								unsubscribeStock(stockUnsubscribeMessage.getClientName(),
										stockUnsubscribeMessage.getStockName());
							}
							else if (stockExchangeMessage instanceof SystemUnregisterMessage) {
								SystemUnregisterMessage systemUnregisterMessage = (SystemUnregisterMessage) stockExchangeMessage;
								removeClient(clientName);
							}

						}
						catch (RuntimeException runtimeException) {
							if (runtimeException instanceof AmazonServiceException) {
								throw runtimeException;
							}
							else {
								// if any runtime exception occured during
								// processing message inform client
								logger.info("Runtime Exception occured while processing message.");

								// make sure this is not the registration queue
								// thread
								if (null != clientName && null != inputQueueUrl && null != outputQueueUrl) {
									logger.info("Sending error message to client {}: {}" + clientName,
											runtimeException.getMessage());
									amazonSQSClient.sendMessage(new SendMessageRequest(inputQueueUrl,
											StockExchangeMessage.encode(new SystemErrorMessage(clientName,
													runtimeException.getMessage()))));
								}
							}
						}
						finally {
							// delete read message from queue
							logger.info("Deleting processed message: {}", message.getReceiptHandle());
							amazonSQSClient.deleteMessage(new DeleteMessageRequest(outputQueueUrl, message
									.getReceiptHandle()));
						}
					}
				}
				catch (AmazonServiceException e) {
					if (!isInterrupted()) {
						logger.error("run()", e); //$NON-NLS-1$
						interrupt();
					}

				}
			}
			logger.info("StockExchangeIOLoop for " + (null == clientName ? clientName : " register queue") + "stopped");
		}

		/**
		 * Register client.
		 * 
		 * @param systemRegisterMessage
		 *            the system register message
		 */
		private void registerClient(SystemRegisterMessage systemRegisterMessage) {
			// create queues for client
			if (null != systemRegisterMessage.getClientName()) {

				String newInputQueueUrl = amazonSQSClient.createQueue(
						new CreateQueueRequest(systemRegisterMessage.getClientName() + "-IN")).getQueueUrl();

				String newOutputQueueUrl = amazonSQSClient.createQueue(
						new CreateQueueRequest(systemRegisterMessage.getClientName() + "-OUT")).getQueueUrl();

				if (null != newInputQueueUrl && null != newOutputQueueUrl) {

					logger.info("Input and output queue for client " + systemRegisterMessage.getClientName()
							+ "created: newInputQueueUrl = [ " + newInputQueueUrl + "], newOutputQueueUrl = ["
							+ newOutputQueueUrl + "]");
					logger.info("Starting client handler thread for client " + systemRegisterMessage.getClientName());

					// create client handler
					StockExchangeIOLoop stockExchangeIOLoop = new StockExchangeIOLoop();
					stockExchangeIOLoop.setClientName(systemRegisterMessage.getClientName());
					stockExchangeIOLoop.setInputQueueUrl(newInputQueueUrl);
					stockExchangeIOLoop.setOutputQueueUrl(newOutputQueueUrl);

					stockExchangeIOLoops.put(systemRegisterMessage.getClientName(), stockExchangeIOLoop);
					// start client handler
					stockExchangeIOLoop.start();

				}
				else {
					logger.info("Couldn't create input and output queue for client "
							+ systemRegisterMessage.getClientName());
				}

			}
			else {
				logger.info("Client name in system register message is null. Didn't create any queues.");
			}
		}

		// usally called by another StockExchangeIOLoop thread (not this) which
		// updated the stock
		/**
		 * Send stock update message.
		 * 
		 * @param stockUpdateMessage
		 *            the stock update message
		 */
		public void sendStockUpdateMessage(StockUpdateMessage stockUpdateMessage) {
			amazonSQSClient.sendMessage(new SendMessageRequest(inputQueueUrl, StockExchangeMessage
					.encode(stockUpdateMessage)));
		}

		/**
		 * Clean up.
		 */
		public void cleanUp() {
			// delete queues, input queue is alway set.
			if (null != inputQueueUrl) {
				logger.info("Removing input queue url {} for client {}.", inputQueueUrl, clientName);
				removeQueue(inputQueueUrl);
				logger.info("Removed.");

			}
			if (null != outputQueueUrl) {
				logger.info("Removing output queue url {} for client {}.", outputQueueUrl, clientName);
				removeQueue(outputQueueUrl);
				logger.info("Removed.");
			}

		}

		/**
		 * Sets the client name.
		 * 
		 * @param clientName
		 *            the new client name
		 */
		private void setClientName(String clientName) {
			this.clientName = clientName;

		}

		/**
		 * Sets the input queue url.
		 * 
		 * @param inputQueueUrl
		 *            the new input queue url
		 */
		public void setInputQueueUrl(String inputQueueUrl) {
			this.inputQueueUrl = inputQueueUrl;
		}

		/**
		 * Sets the output queue url.
		 * 
		 * @param outputQueueUrl
		 *            the new output queue url
		 */
		public void setOutputQueueUrl(String outputQueueUrl) {
			this.outputQueueUrl = outputQueueUrl;
		}

		/**
		 * Checks if is stop.
		 * 
		 * @return true, if is stop
		 */
		public boolean isStop() {
			return stop;
		}

		/**
		 * Sets the stop.
		 * 
		 * @param stop
		 *            the new stop
		 */
		public void setStop(boolean stop) {
			this.stop = stop;
		}
	}

	/**
	 * Inits the stocks.
	 */
	private void initStocks() {
		stocks = new HashMap<StockName, Stock>();
		stocks.put(StockName.AAPL, new Stock(14290000, StockName.AAPL.toString(), 441.81));
		stocks.put(StockName.GOOG, new Stock(2640000, StockName.GOOG.toString(), 879.73));
		stocks.put(StockName.QCOM, new Stock(62100000, StockName.QCOM.toString(), 29.30));
		stocks.put(StockName.EBAY, new Stock(11650000, StockName.EBAY.toString(), 51.45));
		stocks.put(StockName.MSFT, new Stock(39790000, StockName.MSFT.toString(), 35.67));
		stocks.put(StockName.AMZN, new Stock(4580000, StockName.AMZN.toString(), 276.87));
		stocks.put(StockName.ORCL, new Stock(20230000, StockName.ORCL.toString(), 33.82));

		stocksSubscriptions = new HashMap<StockName, List<String>>();
		stocksSubscriptions.put(StockName.AAPL, Collections.synchronizedList(new ArrayList<String>()));
		stocksSubscriptions.put(StockName.GOOG, Collections.synchronizedList(new ArrayList<String>()));
		stocksSubscriptions.put(StockName.QCOM, Collections.synchronizedList(new ArrayList<String>()));
		stocksSubscriptions.put(StockName.EBAY, Collections.synchronizedList(new ArrayList<String>()));
		stocksSubscriptions.put(StockName.MSFT, Collections.synchronizedList(new ArrayList<String>()));
		stocksSubscriptions.put(StockName.AMZN, Collections.synchronizedList(new ArrayList<String>()));
		stocksSubscriptions.put(StockName.ORCL, Collections.synchronizedList(new ArrayList<String>()));
	}

	/**
	 * Removes the queue.
	 * 
	 * @param queueUrl
	 *            the queue url
	 */
	public static void removeQueue(String queueUrl) {
		try {
			amazonSQSClient.deleteQueue(new DeleteQueueRequest(queueUrl));
		}
		catch (AmazonServiceException amazonServiceException) {
			logger.warn("removeQueue(String) - exception ignored", amazonServiceException); //$NON-NLS-1$

		}

	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		SimpleStockExchange sse = new SimpleStockExchange();
		try {
			sse.start();

			System.in.read();

			sse.stop();
			System.exit(0);
		}
		catch (IOException e) {
			logger.error("Unable to start stock exchange", e); //$NON-NLS-1$

		}
		catch (AmazonServiceException e) {
			logger.error("Caught service exception while starting/stopping stock exchange.", e); //$NON-NLS-1$
		}
	}
}
