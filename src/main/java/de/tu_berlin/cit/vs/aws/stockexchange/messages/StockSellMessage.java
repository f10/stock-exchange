package de.tu_berlin.cit.vs.aws.stockexchange.messages;

/**
 * This type shall be used as the sell stocks command send from clients to the server.
 *
 */
public class StockSellMessage extends StockExchangeMessage {

	private static final long serialVersionUID = -2435612193385231491L;
	private int amount = 0;
	private String stockName;
	
	public StockSellMessage(String clientName, String stockName, int amount){
		super(MESSAGE_TYPE.STOCK_SELL, clientName);
		this.stockName = stockName;
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public String getStockName() {
		return stockName;
	}
}
