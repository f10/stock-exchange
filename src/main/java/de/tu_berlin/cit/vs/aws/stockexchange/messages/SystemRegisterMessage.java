package de.tu_berlin.cit.vs.aws.stockexchange.messages;

/**
 * This type shall be used as the register command send from clients to the server.
 *
 */
public class SystemRegisterMessage extends StockExchangeMessage {

	private static final long serialVersionUID = 4932047687183778280L;

	public SystemRegisterMessage(String clientName) {
		super(MESSAGE_TYPE.SYSTEM_REGISTER, clientName);
	}

}
