package de.tu_berlin.cit.vs.aws.stockexchange.messages;

/**
 * This type shall be used as the error notification command send from the server to the clients.
 *
 */
public class SystemErrorMessage extends StockExchangeMessage {

	private static final long serialVersionUID = 3414739689569238146L;
	private String errorMessage;
	
	public SystemErrorMessage(String clientName, String error) {
		super(MESSAGE_TYPE.SYSTEM_ERROR, clientName);
		this.errorMessage = error;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
