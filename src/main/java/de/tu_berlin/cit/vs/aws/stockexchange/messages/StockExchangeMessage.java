package de.tu_berlin.cit.vs.aws.stockexchange.messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import org.apache.commons.codec.binary.Base64;

/**
 * abstract base type for all messages exchanged between client and server
 * 
 * TODO: YOU MUST IMPLEMENT THE ENCODE AND DECODE FUNCTIONS (Hint: you may
 * serialize the message and encode the resulting byte-array with Base64
 * encoding, have a look at org.apache.commons.codec.binary.Base64 which is
 * included in the AWS Java SDK)
 * 
 * @author akliem
 * 
 */
public abstract class StockExchangeMessage implements Serializable {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = LoggerFactory.getLogger(StockExchangeMessage.class);

	public static final String STOCK_EXCHANGE = "StockExchange";

	private static final long serialVersionUID = -6327493102247417L;
	private MESSAGE_TYPE type;

	private String replyID = null;
	private String clientName = null;

	public StockExchangeMessage(MESSAGE_TYPE type, String clientName) {
		this.type = type;
		this.clientName = clientName;
	}

	public String getReplyID() {
		return replyID;
	}

	public void setReplyID(String replyID) {
		this.replyID = replyID;
	}

	public MESSAGE_TYPE getType() {
		return type;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public static String encode(StockExchangeMessage msg) {

		String base64StockExchangeMessage = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(msg);
			base64StockExchangeMessage = Base64.encodeBase64String(bos.toByteArray());
		}
		catch (IOException e) {
			logger.warn("encode(StockExchangeMessage) - exception ignored", e); //$NON-NLS-1$
		}
		finally {

			try {
				out.close();
				bos.close();
			}
			catch (IOException e) {
				logger.error("encode(StockExchangeMessage)", e); //$NON-NLS-1$
			}

		}

		return base64StockExchangeMessage;
	}

	public static StockExchangeMessage decode(String msg) {
		StockExchangeMessage stockExchangeMessage = null;
		ByteArrayInputStream byteArrayInputStream = null;
		ObjectInput objectInput = null;
		try {
			byteArrayInputStream = new ByteArrayInputStream(Base64.decodeBase64(msg));
			objectInput = new ObjectInputStream(byteArrayInputStream);
			objectInput.read();
			stockExchangeMessage = (StockExchangeMessage) objectInput.readObject();
		}
		catch (IOException e) {
			logger.warn("encode(StockExchangeMessage) - exception ignored", e); //$NON-NLS-1$
		}
		catch (ClassNotFoundException e) {
			logger.error("encode(StockExchangeMessage)", e); //$NON-NLS-1$
		}
		finally {

			try {
				objectInput.close();
				byteArrayInputStream.close();
			}
			catch (IOException e) {
				logger.error("encode(StockExchangeMessage)", e); //$NON-NLS-1$

				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return stockExchangeMessage;
	}

	public enum MESSAGE_TYPE {
		STOCK_LIST, STOCK_UPDATE, STOCK_SUBSCRIBE, STOCK_UNSUBSCRIBE, STOCK_BUY, STOCK_SELL,

		SYSTEM_REGISTER, SYSTEM_UNREGISTER, SYSTEM_ERROR
	}
}
