package de.tu_berlin.cit.vs.aws.stockexchange.messages;

/**
 * This type shall be used as the buy stocks command send from clients to the server.
 *
 */
public class StockBuyMessage extends StockExchangeMessage {

	private static final long serialVersionUID = -1070633652178499796L;
	private int amount = 0;
	private String stockName;
	
	public StockBuyMessage(String clientName, String stockName, int amount){
		super(MESSAGE_TYPE.STOCK_BUY, clientName);
		this.stockName = stockName;
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public String getStockName() {
		return stockName;
	}
}
