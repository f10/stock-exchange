package de.tu_berlin.cit.vs.aws.stockexchange;

import java.io.Serializable;

public class Stock implements Serializable{

	private static final long serialVersionUID = -8260300545285526930L;
	private int stockCount;
	private String name;
	private int availableCount;
	private double price;
	
	public Stock(int stockCount, String name, double startingPrice){
		this.stockCount = stockCount;
		this.availableCount = stockCount;
		this.name = name;
		this.price = startingPrice;
	}
	
	public int getStockCount() {
		return stockCount;
	}
	
	protected void setStockCount(int stockCount) {
		this.stockCount = stockCount;
	}
	
	public String getName() {
		return name;
	}
	
	protected void setName(String name) {
		this.name = name;
	}
	
	public int getAvailableCount() {
		return availableCount;
	}
	
	protected void setAvailableCount(int availableCount) {
		this.availableCount = availableCount;
	}
	
	public double getPrice() {
		return price;
	}
	
	protected void setPrice(double price) {
		this.price = price;
	}
	
	public String toString(){
		return "Stock-Name: " + getName() +
			" -- Stock-Price: " + getPrice() + 
			" -- available: " + getAvailableCount() + 
			" -- sum: " + getStockCount();
	}
	
 
}
