package de.tu_berlin.cit.vs.aws.stockexchange.messages;

import de.tu_berlin.cit.vs.aws.stockexchange.Stock;

/**
 * This type shall be used as the stock update notification command send from the server to the clients.
 *
 */
public class StockUpdateMessage extends StockExchangeMessage {

	private static final long serialVersionUID = 3711732384165676623L;
	private Stock updatedStock = null;
	
	public StockUpdateMessage(String clientName, Stock updatedStock) {
		super(MESSAGE_TYPE.STOCK_UPDATE, clientName);
		this.updatedStock = updatedStock;
	}

	public Stock getUpdatedStock() {
		return updatedStock;
	}
}
