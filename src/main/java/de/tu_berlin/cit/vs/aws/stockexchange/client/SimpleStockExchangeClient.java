package de.tu_berlin.cit.vs.aws.stockexchange.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

import de.tu_berlin.cit.vs.aws.stockexchange.SimpleStockExchange;
import de.tu_berlin.cit.vs.aws.stockexchange.Stock;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.StockBuyMessage;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.StockExchangeMessage;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.StockListMessage;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.StockSellMessage;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.StockSubscribeMessage;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.StockUnsubscribeMessage;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.StockUpdateMessage;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.SystemErrorMessage;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.SystemRegisterMessage;
import de.tu_berlin.cit.vs.aws.stockexchange.messages.SystemUnregisterMessage;

/**
 * The Class SimpleStockExchangeClient.
 * 
 * @author Florian Beckmann and others. - florianbeckmann@gmx.net
 */
public class SimpleStockExchangeClient {

	/** Logger for this class. */
	private static final Logger logger = LoggerFactory.getLogger(SimpleStockExchangeClient.class);

	/** The client name. */
	private final String clientName;

	/** The server input queue name. */
	private final String serverInputQueueName;

	/** The server output queue name. */
	private final String serverOutputQueueName;

	/** The input queue url. */
	private String inputQueueUrl = null; // the queue which is used to send
											// commands to the server
	/** The output queue url. */
	private String outputQueueUrl = null; // the queue which is used to receive
	// notifications from the server

	/** The io loop. */
	private IOLoop ioLoop;

	/** The amazon sqs client. */
	private AmazonSQS amazonSQSClient = null;

	/** The register queue url. */
	private String registerQueueUrl;

	/**
	 * Instantiates a new simple stock exchange client.
	 * 
	 * @param clientName
	 *            the client name
	 */
	public SimpleStockExchangeClient(String clientName) {
		if (null == clientName || clientName.isEmpty()) {
			this.clientName = java.util.UUID.randomUUID().toString();
		}
		else {
			this.clientName = clientName;
		}
		/*
		 * If you verify that the server creates the client queues using this
		 * pattern, you can search for them in register()
		 */
		this.serverInputQueueName = this.clientName + "-IN";
		this.serverOutputQueueName = this.clientName + "-OUT";
	}

	/**
	 * Register.
	 * 
	 * @return true, if successful
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private boolean register() throws IOException {
		logger.info("{}", "Starting client registration ... "); //$NON-NLS-1$ //$NON-NLS-2$
		logger.info("{}", "... Done."); //$NON-NLS-1$ //$NON-NLS-2$

		amazonSQSClient = new AmazonSQSClient(new PropertiesCredentials(
				SimpleStockExchange.class.getResourceAsStream("/AwsCredentials.properties")));
		amazonSQSClient.setRegion(com.amazonaws.regions.Region.getRegion(Regions.EU_WEST_1));
		/*
		 * TODO: search for server registration queue here
		 */
		registerQueueUrl = amazonSQSClient.getQueueUrl(new GetQueueUrlRequest(StockExchangeMessage.STOCK_EXCHANGE))
				.getQueueUrl();

		if (null == registerQueueUrl) {
			logger.error("Unable to find registration queue of stock exchange server. Aborting"); //$NON-NLS-1$
			return false;
		}

		logger.info("Registration queue found... registering.");

		/*
		 * register at the server and wait for creation of your client queues
		 */
		amazonSQSClient.sendMessage(new SendMessageRequest(registerQueueUrl, StockExchangeMessage
				.encode(new SystemRegisterMessage(clientName))));
		logger.info("Done registering.");

		for (int i = 0; i < 3; i++) {

			try {
				logger.info("Waiting 3 sec for server to create our IO queues.");
				Thread.sleep(3000);
				if (null == inputQueueUrl) {
					logger.info("Requesting server input queue {}.", serverInputQueueName);
					inputQueueUrl = amazonSQSClient.getQueueUrl(new GetQueueUrlRequest(serverInputQueueName))
							.getQueueUrl();
				}
				if (null == outputQueueUrl) {
					logger.info("Requesting server output queue {}.", serverOutputQueueName);
					outputQueueUrl = amazonSQSClient.getQueueUrl(new GetQueueUrlRequest(serverOutputQueueName))
							.getQueueUrl();
				}

			}
			catch (InterruptedException e) {
				logger.warn("register() - exception ignored", e); //$NON-NLS-1$

			}
			catch (AmazonServiceException amazonServiceException) {
				logger.warn("register() - exception ignored", amazonServiceException); //$NON-NLS-1$

			}

			if (inputQueueUrl != null && outputQueueUrl != null) {
				logger.info("Found server queues: inputQueueUrl = [{}], outputQueueUrl =  [{}]", inputQueueUrl,
						outputQueueUrl);
				ioLoop = new IOLoop();
				ioLoop.start();

				break;
			}
			else if (i < 2) {
				logger.info("Could not find all queues: inputQueueUrl = [{}], outputQueueUrl =  [{}]", inputQueueUrl,
						outputQueueUrl);

				logger.info("Trying again...");

			}
			else if (2==i) {
				logger.error("Unable to find my client queues after trying 3 times! Aborting"); //$NON-NLS-1$
				return false;
			}
		}
		return true;
	}

	/**
	 * Unregister.
	 */
	private void unregister() {
		logger.info("{}", "Logging off ... "); //$NON-NLS-1$ //$NON-NLS-2$

		amazonSQSClient.sendMessage(new SendMessageRequest(outputQueueUrl, StockExchangeMessage
				.encode(new SystemUnregisterMessage(clientName))));
		logger.info("{}", "... Done."); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * List stocks.
	 */
	private void listStocks() {
		logger.info("{}", "Starting stock query ... "); //$NON-NLS-1$ //$NON-NLS-2$
		amazonSQSClient.sendMessage(new SendMessageRequest(outputQueueUrl, StockExchangeMessage
				.encode(new StockListMessage(null, clientName))));
		logger.info("{}", "... Done."); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Subscribe for stock.
	 * 
	 * @param stockName
	 *            the stock name
	 */
	private void subscribeForStock(String stockName) {
		logger.info("{}", "Subsribing for stock " + stockName); //$NON-NLS-1$ //$NON-NLS-2$
		amazonSQSClient.sendMessage(new SendMessageRequest(outputQueueUrl, StockExchangeMessage
				.encode(new StockSubscribeMessage(clientName, stockName))));
		logger.info("{}", "... Done."); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Unsubscribe for stock.
	 * 
	 * @param stockName
	 *            the stock name
	 */
	private void unsubscribeForStock(String stockName) {
		logger.info("{}", "Unsubsribing for stock " + stockName); //$NON-NLS-1$ //$NON-NLS-2$
		amazonSQSClient.sendMessage(new SendMessageRequest(outputQueueUrl, StockExchangeMessage
				.encode(new StockUnsubscribeMessage(clientName, stockName))));
		logger.info("{}", "... Done."); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Buy stock.
	 * 
	 * @param stockName
	 *            the stock name
	 * @param amount
	 *            the amount
	 */
	private void buyStock(String stockName, int amount) {
		logger.info("{}", "Buying " + amount + " stocks of " + stockName); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		amazonSQSClient.sendMessage(new SendMessageRequest(outputQueueUrl, StockExchangeMessage
				.encode(new StockBuyMessage(clientName, stockName, amount))));
		logger.info("{}", "... Done."); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Sell stock.
	 * 
	 * @param stockName
	 *            the stock name
	 * @param amount
	 *            the amount
	 */
	private void sellStock(String stockName, int amount) {
		logger.info("{}", "Selling " + amount + " stocks of " + stockName); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		amazonSQSClient.sendMessage(new SendMessageRequest(outputQueueUrl, StockExchangeMessage
				.encode(new StockSellMessage(clientName, stockName, amount))));
		logger.info("{}", "... Done."); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * The Class IOLoop.
	 */
	private class IOLoop extends Thread {

		/** Logger for this class. */
		private final Logger logger = LoggerFactory.getLogger(IOLoop.class);

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			while (!interrupted()) {

				try {

					// read messages from the server'
					List<Message> messages = amazonSQSClient.receiveMessage(new ReceiveMessageRequest(inputQueueUrl))
							.getMessages();

					if (messages.isEmpty()) {
						try {
							sleep(1000);
						}
						catch (InterruptedException e) {
							logger.error("run()", e); //$NON-NLS-1$

							break;
						}
						continue;
					}

					for (Message message : messages) {
						// decode message
						StockExchangeMessage stockExchangeMessage = StockExchangeMessage.decode(message.getBody());

						if (null != stockExchangeMessage) {
							synchronized (this) {
								logger.info("{}", "+++++++++++++++++++++++++++++++++++++++++++++++"); //$NON-NLS-1$ //$NON-NLS-2$

								switch (stockExchangeMessage.getType()) {
								case STOCK_LIST:
									List<Stock> stocks = ((StockListMessage) stockExchangeMessage).getStocks();
									logger.info("{}", "Received list of available stocks:"); //$NON-NLS-1$ //$NON-NLS-2$
									int count = 1;
									for (Stock stock : stocks) {
										logger.info("{}", "          " + count++ + ": " + stock); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
									}
									break;
								case STOCK_UPDATE:
									Stock stock = ((StockUpdateMessage) stockExchangeMessage).getUpdatedStock();
									logger.info("{}", "Received stock update notification: \n " + stock); //$NON-NLS-1$ //$NON-NLS-2$								
									break;
								case SYSTEM_ERROR:
									logger.info(
											"{}", "Received stock exchange server error message: " + ((SystemErrorMessage) stockExchangeMessage).getErrorMessage()); //$NON-NLS-1$ //$NON-NLS-2$
									break;
								default:
									logger.info(
											"{}", "Received unexpected message type in my server notification queue: " + stockExchangeMessage.getType()); //$NON-NLS-1$ //$NON-NLS-2$
								}

								logger.info("{}", "+++++++++++++++++++++++++++++++++++++++++++++++"); //$NON-NLS-1$ //$NON-NLS-2$
							}

						}
						// delete read message from queue
						logger.info("Deleting processed message: {} from ouput Queue", message.getReceiptHandle());
						amazonSQSClient.deleteMessage(new DeleteMessageRequest(inputQueueUrl, message
								.getReceiptHandle()));
					}
				}
				catch (AmazonServiceException e) {
					if (!isInterrupted()) {
						logger.error("run()", e); //$NON-NLS-1$
						interrupt();
					}
				}
			}
		}

	}

	/**
	 * Prints the help.
	 */
	private static void printHelp() {
		logger.info(
				"{}", "***********************************************\n Options: \n 1 List available stocks \n 2 subscribe for stock notifications\n 3 unsubscribe for stock notifications\n 4 buy stock\n 5 sell stock\n 6 exit\n***********************************************\n Enter a command (Number between 1 and 6):"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		logger.info("{}", "Enter your name: "); //$NON-NLS-1$ //$NON-NLS-2$
		String clientName;
		try {
			clientName = reader.readLine();
		}
		catch (IOException e1) {
			logger.error("main(String[])", e1); //$NON-NLS-1$

			logger.error("Error reading your name. Aborting", e1); //$NON-NLS-1$
			return;
		}

		try {
			SimpleStockExchangeClient simpleStockExchangeClient = new SimpleStockExchangeClient(clientName);
			if (!simpleStockExchangeClient.register()) {
				return;
			}

			boolean exit = false;
			while (!exit) {
				printHelp();
				int command;

				try {
					command = Integer.parseInt(reader.readLine());
				}
				catch (NumberFormatException e) {
					logger.error("Enter a valid command! A number between 1 and 6.", e); //$NON-NLS-1$
					continue;
				}

				logger.info("{}"); //$NON-NLS-1$

				synchronized (simpleStockExchangeClient) {
					switch (command) {
					case 1:
						simpleStockExchangeClient.listStocks();
						break;
					case 2:
					case 3:
						logger.info("{}", "Enter stock name: "); //$NON-NLS-1$ //$NON-NLS-2$
						String stockName = reader.readLine();

						if (command == 2) {
							simpleStockExchangeClient.subscribeForStock(stockName);
						}
						else {
							simpleStockExchangeClient.unsubscribeForStock(stockName);
						}

						break;
					case 4:
					case 5:
						logger.info("{}", "Enter stock name: "); //$NON-NLS-1$ //$NON-NLS-2$
						stockName = reader.readLine();
						logger.info("{}", "Enter amount (positive integer): "); //$NON-NLS-1$ //$NON-NLS-2$
						int amount;
						try {
							amount = Integer.parseInt(reader.readLine());
						}
						catch (NumberFormatException e) {
							logger.error("Given amount is no valid integer.", e); //$NON-NLS-1$
							continue;
						}

						if (command == 4) {
							simpleStockExchangeClient.buyStock(stockName, amount);
						}
						else {
							simpleStockExchangeClient.sellStock(stockName, amount);
						}
						break;
					case 6:
						simpleStockExchangeClient.unregister();
						exit = true;
						break;
					default:
						logger.error("Enter a valid command! A number between 1 and 6."); //$NON-NLS-1$
					}
				}
				logger.info("{}"); //$NON-NLS-1$
			}
		}
		catch (IOException e) {
			logger.error("Caught IOException during operation ... Aborting ", e); //$NON-NLS-1$
			logger.error("main(String[])", e); //$NON-NLS-1$
		}
		catch (AmazonServiceException e) {
			logger.error("Caught amazon service exception during operation ... Aborting", e); //$NON-NLS-1$
			logger.error("main(String[])", e); //$NON-NLS-1$
		}
	}
}
