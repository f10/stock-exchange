package de.tu_berlin.cit.vs.aws.stockexchange.messages;

/**
 * This type shall be used as the unregister command send from clients to the server.
 *
 */
public class SystemUnregisterMessage extends StockExchangeMessage {

	private static final long serialVersionUID = 8476650671158656798L;

	public SystemUnregisterMessage(String clientName) {
		super(MESSAGE_TYPE.SYSTEM_UNREGISTER, clientName);
	}

}
