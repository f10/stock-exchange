package de.tu_berlin.cit.vs.aws.stockexchange.messages;

/**
 * This type shall be used as the subscribe for stock update notifications command send from clients to the server.
 *
 */
public class StockSubscribeMessage extends StockExchangeMessage {
	
	private static final long serialVersionUID = -3425370126686141194L;
	private String stockName;
	
	public StockSubscribeMessage(String clientName, String stockName) {
		super(MESSAGE_TYPE.STOCK_SUBSCRIBE, clientName);
		this.stockName = stockName;
	}

	public String getStockName() {
		return stockName;
	}

}
