package de.tu_berlin.cit.vs.aws.stockexchange.messages;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.tu_berlin.cit.vs.aws.stockexchange.Stock;

/**
 * This type shall be used both as the list stocks command send from clients to
 * the server (leave stocks parameter blank/null) and as the result notification
 * send from the server to the clients.
 * 
 */
public class StockListMessage extends StockExchangeMessage {

	private static final long serialVersionUID = 7062676640030242364L;
	private List<Stock> stocks = null;

	public StockListMessage(LinkedList<Stock> stocks, String clientName) {
		super(MESSAGE_TYPE.STOCK_LIST, clientName);
		this.stocks = stocks;
	}

	public List<Stock> getStocks() {
		if (stocks != null)
			return Collections.unmodifiableList(stocks);
		else
			return null;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}

}
