package de.tu_berlin.cit.vs.aws.stockexchange.messages;

/**
 * This type shall be used as the unsubscribe for stock update notifications command send from clients to the server.
 *
 */
public class StockUnsubscribeMessage extends StockExchangeMessage {

	private static final long serialVersionUID = -3528527979230050035L;
	private String stockName;
	
	public StockUnsubscribeMessage(String clientName, String stockName) {
		super(MESSAGE_TYPE.STOCK_UNSUBSCRIBE, clientName);
		this.stockName = stockName;
	}

	public String getStockName() {
		return stockName;
	}
}
