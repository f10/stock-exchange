#!/bin/sh

######### stuff for downloading gradle build system ###########
GRADLE_VERSION=1.6
GRADLE_BASE=gradle-$GRADLE_VERSION
GRADLE_ARCHIVE=$GRADLE_BASE-bin.zip
GRADLE_HOME=./$GRADLE_BASE
#export GRADLE_USER_HOME=./.gradle
GRADLE_MIRROR=http://services.gradle.org/distributions/$GRADLE_ARCHIVE

GRADLE_BIN=$GRADLE_HOME/bin/gradle



FINAL_JAR=./build/libs/stock-exchange-0.1-SNAPSHOT.jar

if [ ! -f $GRADLE_BIN ]
then
	echo "Local gradle not installed."
	if [ ! -f $GRADLE_ARCHIVE ]
	then
		echo "Fetching binaries from mirror $GRADLE_MIRROR"
		wget $GRADLE_MIRROR
	fi
	echo "unpacking."
	unzip ./$GRADLE_ARCHIVE
	echo "done."
fi


#############################################################################
# Change the following two variables
#############################################################################

# compile your code, if necessary
# leave this blank if you do not need to compile code
#COMPILECMD="$GRADLE_BIN -g $GRADLE_USER_HOME clean cleanEclipse eclipse build jar "
COMPILECMD="$GRADLE_BIN clean cleanEclipse eclipse build jar "

# start your stock-exchange 
RUNCMD="java -jar $FINAL_JAR"

#############################################################################
if [ ! -f $FINAL_JAR ]
then
	echo "no binary found... starting build system."
	echo $COMPILECMD
  $COMPILECMD
fi
# you can use for example ./run.sh -p 1234 -b 192.168.0.4
# type ./run.sh -h for help
$RUNCMD $@
